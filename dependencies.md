Dependencies:

- Python (required)
- Shell Script support (required for the installer scripts). Sadly, we haven't figured out windows support yet due to the way mountable drives are handled, but you can still make it yourself and send us a merge request, which would be a huge help
- Remaining dependancies should be installed by the installer script
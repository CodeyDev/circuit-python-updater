#!/bin/bash
echo Starting Setup...
python3 -V
sudo pip3 install BeautifulSoup4
sudo pip3 install requests
sudo mkdir /usr/share/cpyupdate/
cd "$( dirname "$0" )"
sudo cp ../../src/img/updater.png /usr/share/cpyupdate/
echo Done!
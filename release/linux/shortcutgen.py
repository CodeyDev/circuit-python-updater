#!/usr/bin/env python3
#Shortcut Generator for the Circut Python Updater

f = open("Circut Python Updater.desktop","wt")
programLoc = str(input("Enter The Path To The cpyupdate.py File: "))
contents = ["""[Desktop Entry]
Name=Circut Python Updater
Comment=Update your Circuit Python boards in a snap!
Icon=/usr/share/cpyupdate/updater.png
Exec=""",programLoc,"\n","""Type=Application
Encoding=UTF-8
Terminal=true"""]

content = contents[0] + contents[1] + contents[2] + contents[3]

f.write(content)

f.close()